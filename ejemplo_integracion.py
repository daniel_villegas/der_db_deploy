# esto es importante para acceder al ORM de django desde afuera

from __future__ import absolute_import, unicode_literals
import os
import datetime
import pytz

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'embc_app.settings')
import django
django.setup()



from der import models

# la vpp consulta los DER que pueden despacharse
models.DER.get_dispatched_ders()

# la VPP reporta el valor del despacho de un DER conocido, es importante usar el slug para identificar los DER 
tz = pytz.timezone("America/Bogota")
# consultamos un DER
a=models.DER.objects.all()[0]
date = datetime.datetime.utcnow()
date.replace(tzinfo=tz)
# le asignamos un registro de despacho ()
a.set_dispatch_value(2, date)