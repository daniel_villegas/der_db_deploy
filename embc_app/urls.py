"""embc_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from der import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('ders/', views.DerListView.as_view(),  name='list-ders'),
    path('ders/registro/', views.DerCreateForUserView.as_view(), name='reg-der'),
    path('ders/<slug:slug>/', views.DerDetailView.as_view(), name='dtl-der'),
    path('ders/<slug:slug>/editar', views.DerUpdateView.as_view(), name='edt-der'),
    path('ders/<slug:slug>/eliminar', views.DerUpdateView.as_view(), name='del-der'),
    path('ders/<slug:slug>/daily', views.get_daily_report, name='daily-der-report'),
    path('ders/aggregated', views.get_aggregated_report, name='aggregated-der-report'),
    path('', views.DashboardView.as_view(), name="dashboard")
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
