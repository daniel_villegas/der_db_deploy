# Generated by Django 2.2 on 2019-09-20 20:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('der', '0002_auto_20190920_2039'),
    ]

    operations = [
        migrations.RenameField(
            model_name='der',
            old_name='despachada',
            new_name='dispatched',
        ),
    ]
