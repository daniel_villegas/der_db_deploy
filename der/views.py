import datetime
import pytz
import math

from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from django.urls import reverse_lazy

from .models import DER, DERMeasurements, Dispatch
from django.http import JsonResponse
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from timezonefinder import TimezoneFinder
from django.views.decorators.http import require_GET
from pandas.core.groupby.groupby import DataError

import pandas as pd
import numpy as np

# Create your views here.



class DashboardView(LoginRequiredMixin, TemplateView):
    login_url = '/accounts/login/'
    template_name = "base/dashboard.html"

class DerListView(LoginRequiredMixin, ListView):
    login_url = '/accounts/login/'
    template_name = "ders/list.html"
    model = DER
    def get_queryset(self):
        r = self.model.objects.filter(owner=self.request.user)
        return r

class DerCreateView(LoginRequiredMixin, CreateView):
    login_url = '/accounts/login/'
    template_name = "ders/create.html"
    model = DER
    success_url = reverse_lazy('list-ders')
    fields = ('der_type', "neighborhood", "city", "departament", "location", "rated_capacity", "use_type", "dispatched")

class DerUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/accounts/login/'
    template_name = "ders/update.html"
    model = DER
    success_url = reverse_lazy('list-ders')
    fields = ('der_type', "neighborhood", "city", "departament", "location", "rated_capacity", "use_type", "dispatched")

class DerDetailView(LoginRequiredMixin, DetailView):
    login_url = '/accounts/login/'
    template_name = "ders/detail.html"
    model = DER
    success_url = reverse_lazy('list-ders')
    fields = ('der_type', "neighborhood", "city", "departament", "location", "rated_capacity", "use_type", "dispatched")



class DerDeleteView(LoginRequiredMixin, CreateView):
    login_url = '/accounts/login/'
    template_name = "ders/create.html"
    model = DER
    success_url = reverse_lazy('list-ders')
    fields = ('der_type', "neighborhood", "city", "departament", "location", "rated_capacity", "use_type")




class DerCreateForUserView(DerCreateView):
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(DerCreateForUserView, self).form_valid(form)


def test_view(request):
    return HttpResponse("hola")


fields = [
    'generated-active-power',
    'generated-reactive-power',
    'solar-irradiation',
    'temperature'
]


def align_timestamp_to_delta(timestamp, delta):
    return math.ceil(timestamp / delta) * delta



def get_dates(query_dict, target_tz):
    query_date = query_dict.get('date', None)
    if query_date is None:
        query_date = datetime.datetime.now(pytz.timezone(target_tz))
    else:
        query_date = datetime.datetime.strptime(query_date, "%Y-%m-%d")
        query_date = datetime.datetime.combine(query_date, datetime.datetime.min.time())

    today_local = query_date.replace(hour=0, minute=0, second=0, microsecond=0)
    
    tomorrow_local = today_local + datetime.timedelta(1)

    today_utc = today_local.astimezone(pytz.timezone('UTC'))
    tomorrow_utc = tomorrow_local.astimezone(pytz.timezone('UTC'))
    return (today_utc, tomorrow_utc)

@require_GET
@login_required
def get_daily_report(request, slug):
    try:
        der = DER.objects.get(slug=slug, owner=request.user)
    except ObjectDoesNotExist:
        raise Http404("Der no existe")
    
    tf = TimezoneFinder()
    latitude, longitude = tuple(map(float, der.location.split(',')))
    der_tz = tf.timezone_at(lng=longitude, lat=latitude)


    today_utc, tomorrow_utc = get_dates(request.GET, der_tz)
    
    objects = list(DERMeasurements.objects.filter(der=slug, timestamp__lte=tomorrow_utc, timestamp__gte=today_utc).all())

    dispatch = list(Dispatch.objects.filter(der=slug, timestamp__lte=tomorrow_utc, timestamp__gte=today_utc).order_by("timestamp").all())
    print("objects here", objects)
    try:
        data = pd.DataFrame([[align_timestamp_to_delta(datetime.datetime.timestamp(i.timestamp), 60*5)] + [i.samples.get(key, None) for key in fields] for i in objects], columns=['timestamp'] + fields)
        dispatch = [{'timestamp' : align_timestamp_to_delta(obj.timestamp.timestamp(), 60*5), 'active-power-sp' : obj.active_power} for obj in dispatch]
        print(dispatch)
        dispatch = pd.DataFrame([[i['timestamp'],i['active-power-sp']] for i in dispatch], columns=['timestamp',  'active-power-sp'])
        print(dispatch)
        data = pd.merge(dispatch,data, on="timestamp", how="outer")
        
        data[['active-power-sp']] = data[['active-power-sp']].fillna(0)
        data[fields] = data[fields].fillna(method='ffill')
        print("with empty join",data)
        data = data.groupby('timestamp', as_index=False).mean().sort_values('timestamp')
        print(data)
        data = data.fillna(0)
        r = data.to_dict(orient="split")
        return JsonResponse(r)
    except DataError:
        raise Http404


field_aggregation = {
    'generated-active-power' : ['sum'],
    'generated-reactive-power' : ['sum'],
    'solar-irradiation' : ['mean', 'std'],
    'temperature' : ['mean', 'std']
}

@require_GET
@login_required
def get_aggregated_report(request):
    ders = list(request.user.der_set.values("slug", "location"))
    tf = TimezoneFinder()
    latitude, longitude = tuple(map(float, ders[0]['location'].split(',')))
    der_tz = tf.timezone_at(lng=longitude, lat=latitude)

    today_utc, tomorrow_utc = get_dates(request.GET, der_tz)
    objects = list(DERMeasurements.objects.filter(
        der__in=map(lambda x: x['slug'], ders),
        timestamp__lte=tomorrow_utc,
        timestamp__gte=today_utc).all()
    )
    try:
        agg_funcs = dict(Size='size', Sum='sum', Mean='mean', Std='std')
        data = pd.DataFrame([[align_timestamp_to_delta(datetime.datetime.timestamp(i.timestamp), 60*15), i.der] + [i.samples.get(key, None) for key in fields] for i in objects], columns=['timestamp', 'der'] + fields)
        data = data.groupby(['timestamp', 'der'], as_index=False).agg("mean").drop("der", axis=1)
        print(data)
        data = data.groupby('timestamp', as_index=False).agg(field_aggregation).sort_values('timestamp')
        r = data.to_dict(orient="split")
        #r['columns'] = list(map(lambda x: '-'.join(x), r['columns']))
        #r['columns'][0] = r['columns'][0][:-1] 
        return JsonResponse(r)
    except DataError:
        raise Http404
    