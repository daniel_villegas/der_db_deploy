from django.contrib import admin
from .models import DER, DERMeasurements



# Register your models here.
class DerMeasurementsAdmin(admin.ModelAdmin):
    list_display = ('der', 'arrival_timestamp', 'timestamp', 'samples')

admin.site.register(DERMeasurements, DerMeasurementsAdmin)

# Register your models here.
class DerRegistryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("der_type", "city", "neighborhood", "use_type", "rated_capacity")}
    list_display = ('der_type', 'use_type', 'is_solar')

    def is_solar(self, obj):
        if obj.der_type == 'SOL':
            return "Si"
        return "No"

admin.site.register(DER, DerRegistryAdmin)