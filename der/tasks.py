from __future__ import absolute_import, unicode_literals
import os
import datetime
import pytz
from celery import task
from timezonefinder import TimezoneFinder


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'embc_app.settings')
import django
django.setup()

from der import models
from django.core.exceptions import ObjectDoesNotExist

allowed_fields = [
    'generated-active-power',
    'generated-reactive-power',
    'solar-irradiation',
    'temperature'
]

def save_to_database(message, der_id):
    tf = TimezoneFinder()
    try:
        der = models.DER.objects.get(slug=der_id)
    except ObjectDoesNotExist:
        return
    
    latitude, longitude = tuple(map(float, der.location.split(',')))
    der_tz = tf.timezone_at(lng=longitude, lat=latitude)
    der_tz = pytz.timezone(der_tz)
    print(der_tz)
    message_timestamp = message.get('timestamp', None)
    if message_timestamp is None:
        return
    m = models.DERMeasurements(
        der = der_id,
        timestamp=datetime.datetime.fromtimestamp(message_timestamp, tz=pytz.timezone('UTC')),
        samples = {i:message[i] for i in message if i in allowed_fields}
        )
    m.save()


@task()
def receive_der_measurement(message_dict, der_id):
    save_to_database(message_dict, der_id)

# todo:
# arreglar zonas horarias