from django.db import models
from django.utils.text import slugify
from django.contrib.postgres.fields import JSONField
from location_field.models.plain import PlainLocationField
from django.contrib.auth.models import User
from django.urls import reverse


# Create your models here.

class DER(models.Model):
    slug = models.SlugField(unique=True, max_length=100)
    owner = models.ForeignKey(User,
                              on_delete=models.CASCADE)
    DER_TYPES = (
        ("SOL", "Solar"),
        ("EOL", "Eólica"),
        ("COG", "Cogenerador"),
        ("PCH", "Pequeña central hidroeléctrica"),
    )
    USE_TYPES = (
        ("RES", "Residencial"),
        ("COM", "Comercial"),
    )
    der_type = models.CharField(
        max_length = 3,
        choices = DER_TYPES
    )
    neighborhood = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    departament = models.CharField(max_length=100)
    location = PlainLocationField(default='4.6486259,-74.2478918')
    rated_capacity = models.PositiveIntegerField()
    use_type = models.CharField(
        max_length = 3,
        choices = USE_TYPES
    )
    dispatched = models.BooleanField(default=False)

    @classmethod
    def get_dispatched_ders(cls):
        return cls.objects.filter(dispatched=True).all()

    def set_dispatch_value(self, value, timestamp):
        obj = Dispatch(der = self.slug, active_power=value, timestamp=timestamp)
        obj.save()

    def save(self, *args, **kwargs):
        if self.pk is None:
            # new object
            if self.slug == "":
                self.slug = slugify("%s %s %s %s %s" % (
                    self.get_der_type_display(),
                    self.city,
                    self.neighborhood,
                    self.get_use_type_display(),
                    self.rated_capacity
                    )
                )
        super(DER, self).save(*args, **kwargs)

class Dispatch(models.Model):
    der  = models.SlugField(max_length=100)
    active_power = models.FloatField(default=0)
    timestamp = models.DateTimeField()

class DERMeasurements(models.Model):
    der  = models.SlugField(max_length=100)
    arrival_timestamp = models.DateTimeField(auto_now_add=True)
    timestamp = models.DateTimeField()
    samples = JSONField()