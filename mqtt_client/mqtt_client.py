import datetime
import json

import paho.mqtt.client as mqtt
from pymongo import MongoClient
from bson.objectid import ObjectId
from bson.errors import InvalidId
from settings import *

from celery import Celery


app = Celery('embc_app')
app.conf.broker_url = 'redis://localhost:6379/0'



def save_measurement_to_db(der_id, message_date, message):
    #check if DER exists
    try:
        der_id = ObjectId(der_id)
    except InvalidId:
        print("invalid client id")
        return
    
    mongo_client = MongoClient(mongo_host, username=mongo_username, password=mongo_password)
    db=mongo_client.get_database(mongo_db_name)

    der_dermeasurements = db.der_dermeasurements
    ders = db.der_der
    der = ders.find_one({'_id' : der_id})
    if der is None:
        print("der does not exist")
        return
    

    # check if measurement exists
    measurement_query = {
        'der' : ObjectId(der_id),
        'date' : datetime.datetime.strftime(message_date, "%Y-%m-%d")
    }

    observation = {
                        "timestamp" : datetime.datetime.timestamp(message_date),
                        "active_power_generated" : float(message.get("active_power_generated", 0)),
                        "reactive_power_generated" : float(message.get("reactive_power_generated", 0))
                    }
    measurement = der_dermeasurements.find_one(measurement_query)
    result = der_dermeasurements.find_one_and_update(
            measurement_query,
            {
                '$push' : {
                    'samples' : observation
                }
            }
            )
    
    if result is None:
        der_dermeasurements.insert_one(
            {
                'der' : ObjectId(der_id),
                'date' : datetime.datetime.strftime(message_date, "%Y-%m-%d"),
                'samples' : [
                    {
                        "active_power_generated" : float(message.get("active_power_generated", 0)),
                        "reactive_power_generated" : float(message.get("reactive_power_generated", 0))
                    }
                ]
            }
        )
        

    print(der_id)
    #save_measurement_to_db(der_id, message_date, message)

    

#save_measurement_to_db()




def on_connect(client, userdata, flags, rc):
   print("Connected With Result Code ", rc)

def on_message(client, userdata, message):
    print("message arrived %s" % message.payload)
    try:
        json_msg = json.loads(message.payload)
        client_id = message.topic.split('/')[-1]
    except Exception as e:    
        print(e)
        return
    app.send_task('der.tasks.receive_der_measurement', (json_msg, client_id))


client = mqtt.Client(client_id="hello")
client.username_pw_set(username="sstr", password="Isaac1234")
client.on_connect = on_connect
client.on_message = on_message
client.connect(broker_url, broker_port)
client.subscribe("ders/+")
client.message_callback_add("ders/+", on_message)

client.loop_forever()
